<?php

require_once 'Animal.php';

Class Frog extends Animal{
	
	protected $jumps = "Hop Hop";

	public function jump()
	{
		return $this->jumps;
	}
}

?>