<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//param pertama untuk url
//view untuk ke view.blade

Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function(){
	return view('page.table');
});

Route::get('/data-table', function(){
	return view('page.datatable');
});

//Cast CRUD

//Add

Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);

//Read

Route::get('/cast', [CastController::class, 'index']);

//Get per ID

Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Edit

Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete

Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);