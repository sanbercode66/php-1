@extends('layouts.master')

@section('title')

Buat Account Baru!

@endsection

@section('content')

<h3>Sign Up Form</h3>

<form action="/welcome" method="POST">

@csrf

<label>First name : </label> <br><br>
<input type="text" name="firstname"> <br><br>

<label>Last name : </label> <br><br>
<input type="text" name="lastname"> <br><br>

<label>Gender : </label><br><br>
<input type="radio" name="gender"> Male <br>
<input type="radio" name="gender"> Female <br>
<input type="radio" name="gender"> Other <br>

<br>

<label for="nationality">Nationality : </label> <br><br>
<select name="nationality">
	<option value="Indonesian">Indonesian</option>
	<option value="Singapuran">Singapuran</option>
	<option value="Malaysian">Malaysian</option>
	<option value="Australian">Australian</option>
</select>

<br><br>

<label for="Language Spoken">Language Spoken : </label> <br><br>

<input type="checkbox" name="language"> Bahasa Indonesia <br>
<input type="checkbox" name="language"> English <br>
<input type="checkbox" name="language"> Other <br>

<br><br>

<label for="bio">Bio :</label> <br><br>

<textarea name="bio" cols="30" rows="10"></textarea>

<br>

<input type="submit" value="Sign Up">

</form>

@endsection