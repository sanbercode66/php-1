@extends('layouts.master')

@section('title')

Data Cast

@endsection

@section('content')

<form action="/cast/{{$editCast->id}}" method="POST">

	@csrf
	@method('put')

  <div class="form-group">
    <label>Nama</label>
    <input type="text" class="form-control" name="nama" value="{{ $editCast->nama }}">
  </div>
  @error('nama')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" class="form-control" name="umur" value="{{ $editCast->umur }}">
  </div>
  @error('umur')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <br>
    <textarea name="bio" cols="50" rows="10">{{ $editCast->bio }}</textarea>
  </div>
  @error('bio')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection