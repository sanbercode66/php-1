<?php

require 'Animal.php';
require 'Ape.php';
require 'Frog.php';

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name; // "shaun"
echo "<br>Legs : " . $sheep->legs; // 4
echo "<br>Cold Blooded : " . $sheep->cold_blooded; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

echo "<br><br>";

$kodok = new Frog("buduk");

echo "Name : " . $kodok->name; // "buduk"
echo "<br>Legs : " . $kodok->legs; // 4
echo "<br>Cold Blooded : " . $kodok->cold_blooded; // "no"
echo "<br>Jump : " . $kodok->jump();// "Hop Hop"

echo "<br><br>";

$sungokong = new Ape("kera sakti");

echo "Name : " . $sungokong->name; // "kera sakti"
echo "<br>Legs : " . $sungokong->legs; // 2
echo "<br>Cold Blooded : " . $sungokong->cold_blooded; // "no"
echo "<br>Yell : " . $sungokong->yell() // "Auooo"

?>