@extends('layouts.master')

@section('title')

Data Cast

@endsection

@section('content')

<a href="/cast/create" class="btn btn-sm btn-primary my-3">Create</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
  	@forelse($data as $key => $items)
    <tr>
    	<td>{{ $key + 1 }}</td>
    	<td>{{ $items->nama }}</td>
    	<td>{{ $items->umur }}</td>
    	<td>{{ $items->bio }} </td>
    	<td>
    		<form action="/cast/{{$items->id}}" method="POST">

    		<a href="/cast/{{$items->id}}" class="btn btn-sm btn-success">Detail</a>
    		<a href="/cast/{{$items->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
    		
    			@csrf
    			@method('delete')

    			<input type="submit" value="Delete" class="btn btn-danger btn-sm">

    		</form>
    	</td>
    </tr>
    @empty

    <h1>Data Kosong</h1>

    @endforelse
  </tbody>
</table>

@endsection