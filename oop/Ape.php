<?php

require_once 'Animal.php';

Class Ape extends Animal{
	
	public $legs = 2;
	protected $sound = "Auooo";

	public function yell()
	{
		return $this->sound;
	}
}

?>